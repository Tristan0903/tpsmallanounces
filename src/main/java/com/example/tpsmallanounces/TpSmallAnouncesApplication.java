package com.example.tpsmallanounces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpSmallAnouncesApplication {

    public static void main(String[] args) {
        SpringApplication.run(TpSmallAnouncesApplication.class, args);
    }

}
