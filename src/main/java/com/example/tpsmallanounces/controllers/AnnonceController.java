package com.example.tpsmallanounces.controllers;

import com.example.tpsmallanounces.entities.Annonce;
import com.example.tpsmallanounces.entities.Category;
import com.example.tpsmallanounces.repositories.AnnonceRepository;
import com.example.tpsmallanounces.services.AnnonceService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("annonce")
public class AnnonceController {

    @Autowired
    AnnonceService _annonceService;

    @Autowired
    HttpServletResponse _response;

    @GetMapping("")
    public ModelAndView getAnnonces(){
        ModelAndView mv = new ModelAndView("accueil");
        List<Annonce> annonceList = new ArrayList<>();
        annonceList.addAll(_annonceService.getAll());
        mv.addObject(annonceList);
        return mv;
    }

    @GetMapping("create")
    public ModelAndView getAnnonceForm(){
        ModelAndView mv = new ModelAndView("annonce-form");
        return mv;
    }

    @PostMapping("submit-annonce")
    public ModelAndView submitAnnonce(@RequestParam String title, @RequestParam String description, @RequestParam List<MultipartFile> images, @RequestParam List<Category> categories) throws Exception {
        ModelAndView mv = new ModelAndView("annonce-form");
        if(title != null && description != null && images == null && categories == null){
            _annonceService.create(title, description);
            _response.sendRedirect("/annonce");
        }
        else if(title != null && description != null && images != null && categories!= null){
            _annonceService.create(title, description, images, categories);
            _response.sendRedirect("/annonce");
        }
        return mv;
    }



}
