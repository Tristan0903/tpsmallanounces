package com.example.tpsmallanounces.repositories;

import com.example.tpsmallanounces.entities.Annonce;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnonceRepository extends CrudRepository<Annonce, Integer> {


    public Annonce findAnnonceByTitle(String title);

    public Annonce findAnnonceById(int id);


}
