package com.example.tpsmallanounces.services;

import com.example.tpsmallanounces.entities.User;
import com.example.tpsmallanounces.repositories.UserRepository;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    HttpSession _httpSession;

    @Autowired
    UserRepository _userRepository;

    public boolean register(String firstName, String lastName, String phone, String email, String password){
        if(_userRepository.searchUserByEmail(email) == null){
            User user = new User();
            user.setFirstname(firstName);
            user.setLastname(lastName);
            user.setEmail(email);
            user.setPhone(phone);
            user.setPassword(password);
            if(_userRepository.save(user) != null){
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean login(String email, String password){
        User user = _userRepository.searchUserByEmail(email);
        if(user != null && user.getPassword().equals(password)) {
            _httpSession.setAttribute("isLogged", "OK");
            return true;
        }
        return false;
    }

    public boolean isLogged() {
        try {
            String attrIsLogged = _httpSession.getAttribute("isLogged").toString();
            return attrIsLogged.equals("OK");
        }catch (Exception ex) {
            return false;
        }
    }
}
