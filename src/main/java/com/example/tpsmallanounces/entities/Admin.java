package com.example.tpsmallanounces.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Data;

@Entity
@Data
public class Admin extends User{

    @Column(name = "is_admin")
    private boolean isAdmin;
}
